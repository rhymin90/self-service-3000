export function delay(ms: number) {
  return new Promise((t) => {
    setTimeout(t, ms);
  });
}
