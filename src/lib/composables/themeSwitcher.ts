export const setTheme = (theme: string) => {
  document.documentElement.setAttribute('theme', theme);
};
