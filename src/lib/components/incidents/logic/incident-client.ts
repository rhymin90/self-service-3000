// import 'whatwg-fetch';

import { Fetcher } from 'openapi-typescript-fetch';

import type { paths } from '../../../../generated/incidents';

// declare fetcher for paths
const fetcher = Fetcher.for<paths>();

// global configuration
fetcher.configure({
  baseUrl: 'http://localhost:3030/api/v1',
});

// create fetch operations
const fetchAllIncidents = fetcher.path('/incidents').method('get').create();

export { fetchAllIncidents };
