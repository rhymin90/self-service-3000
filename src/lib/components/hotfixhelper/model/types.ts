import type { DataTableRow } from 'carbon-components-svelte/types/DataTable/DataTable.svelte';

export interface VersionedContainer {
  container: string;
  version: string;
}

export interface VersionedContainerDataRow extends VersionedContainer, DataTableRow {}

export enum PostState {
  inactive = 'inactive',
  active = 'active',
  finished = 'finished',
}
