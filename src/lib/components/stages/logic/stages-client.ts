// import 'whatwg-fetch';

import { Fetcher } from 'openapi-typescript-fetch';

import type { paths } from '../../../../generated/stages';

// declare fetcher for paths
const fetcher = Fetcher.for<paths>();

// global configuration
fetcher.configure({
  baseUrl: 'http://localhost:8080/api/v1',
});

// create fetch operations
const fetchAllStages = fetcher.path('/stages').method('get').create();

export { fetchAllStages };
