import { sveltekit } from '@sveltejs/kit/vite';
import { optimizeCss } from 'carbon-preprocess-svelte';
const production = process.env.NODE_ENV === 'production';

/** @type {import('vite').UserConfig} */
const config = {
  optimizeDeps: {
    include: ['@carbon/charts'],
  },
  ssr: {
    noExternal: [production && '@carbon/charts'].filter(Boolean),
  },
  plugins: [sveltekit(), production && optimizeCss()],
};

export default config;
